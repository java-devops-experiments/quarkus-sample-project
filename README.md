This project creates a Gitlab Pages site containing a 
[Swagger](https://java-devops-experiments.gitlab.io/quarkus-sample-project/)
of the repository Quarkus project.